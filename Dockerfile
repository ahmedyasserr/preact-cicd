FROM node:14

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json .
COPY package-lock.json .
RUN npm install --silent

# copy source code
COPY . .

# Expose port
EXPOSE 5173
# Start the app
CMD [ "npm", "run dev" ]